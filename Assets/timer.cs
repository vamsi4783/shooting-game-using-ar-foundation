﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
public class timer : MonoBehaviour
{
	int countDownStartValue = 120;
	public Text TimerUI;

	void Start()
	{
		countDownTimer();
	}

	void countDownTimer()
	{
		if (countDownStartValue > 0)
		{
			TimeSpan spanTime = TimeSpan.FromSeconds(countDownStartValue);
			TimerUI.text = "Timer:" + spanTime.Minutes + " : " + spanTime.Seconds;
			countDownStartValue--;
			Invoke("countDownTimer", 1.0f);
		}
		else
		{
			TimerUI.text = "GAME OVER !";
		}
	}
}