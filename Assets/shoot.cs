﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class shoot : MonoBehaviour
{
	// Start is called before the first frame update
	public GameObject arCamera;
	public GameObject smoke;

	public void Shoot()
	{
		RaycastHit hit;
		if (Physics.Raycast(arCamera.transform.position, arCamera.transform.forward, out hit))
		{
			if (hit.transform.name == "point1(Clone)" || hit.transform.name == "point2(Clone)" || hit.transform.name == "point3(Clone)")
			{
				Destroy(hit.transform.gameObject);

				Instantiate(smoke, hit.point, Quaternion.LookRotation(hit.normal));
			}
		}
	}
}
