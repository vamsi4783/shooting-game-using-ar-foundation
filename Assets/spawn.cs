﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spawn : MonoBehaviour
{
    public Transform[] spawnPoints;
    public GameObject[] zombie;
    

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(StartSpawning());
    }

    // Update is called once per frame
    IEnumerator StartSpawning()
    {
        yield return new WaitForSeconds(5);

        for (int i = 0; i < 10; i++)

        {
            int randomNumber = Mathf.RoundToInt(Random.Range(0f, spawnPoints.Length - 1));
            Instantiate(zombie[i], spawnPoints[i].position, Quaternion.identity);
        }
 
        StartCoroutine(StartSpawning());
    }
    void Update()
    {

    }
}
